import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	
	
	getUserFavoriteType: option => $http.get("/v1/getUserFavoriteType", option),
	getUserFavorite: option => $http.get("/v1/getUserFavorite", option),
	
	// get: option => $http.get("/v1/get", option),http://shopTopApp.com/v1/?pageSize=20&page=1&type_id=&time=month
	// post: option => $http.post("/v1/post", option),
	
}