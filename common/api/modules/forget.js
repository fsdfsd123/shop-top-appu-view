import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	
	getVerifyCode: option => $http.get("/v1/getVerifyCode", option),
	resetPassword: option => $http.post("/v1/resetPassword", option),
	
}