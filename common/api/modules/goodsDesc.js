import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	guestReplyCount: options => $http.get('/guestbook/guestReplyCount', options),
	queryByPage1: options => $http.get('/guestbook/queryByPage1', options),
	
	// get: option => $http.get("/v1/index", option),
	// getUserInfo: option => $http.get("/v1/getUserInfo", option),
	// getCategoryList : option => $http.get("/v1/getCategoryList ", option),
	// getGoodsQuality: option =>$http.get("/v1/getGoodsQuality", option),
	// getUserBuy: option =>$http.get("/v1/getUserBuy", option),
	// getUserFavorite: option =>$http.get("/v1/getUserFavorite", option),
	// getUserHobby: option =>$http.get("/v1/getUserHobby", option),
	   getGoodsInfo: option =>$http.get("/v1/getGoodsInfo",option),
	// getGoodsComment: option =>$http.get("/v1/getGoodsComment",option),
	favoriteSave: option =>$http.get("/v1/favoriteSave",option),
	likeSave: option =>$http.get("/v1/likeSave",option),
	buySave: option =>$http.get("/v1/buySave",option),
	shareSave: option =>$http.get("/v1/shareSave",option),
	
	// // post
	// login: option => post("/v1/login", option),
	 commentSave: option => $http.post("/v1/commentSave", option),
}