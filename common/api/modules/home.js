import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	getGoodsQuality: option => $http.get("/v1/getGoodsQuality", option),
	
	
	
	// home-recommend
	getGoodsRecommend: options => $http.get('/v1/getGoodsRecommend', options),
	
	
	getCategoryList: option => $http.get("/v1/getCategoryList", option),
	
}
