import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	login: options => $http.post('/v1/login', options),
	getUserInfo: option => $http.get("/v1/getUserInfo", option),

	enrolls: option => $http.post("/v1/enrolls", option),
	enrollVerifyCode: option => $http.get("/v1/enrollVerifyCode", option),
	
	memberSave: option => $http.post("/v1/memberSave", option),
	mySave: option => $http.post("/v1/mySave", option),
	
}
