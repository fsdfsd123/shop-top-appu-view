import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	
	uploadAvatar: option => $http.upload("/v1/uploadAvatar", option),
	getLikeType: option => $http.get("/v1/getLikeType", option),
	getLike: option => $http.get("/v1/getLike", option),
	getShareType: option => $http.get("/v1/getShareType", option),
	getUserBuy: option => $http.get("/v1/getUserBuy", option),
	getShare: option => $http.get("/v1/getShare", option),
	getUserBuyType: option => $http.get("/v1/getUserBuyType", option),
	getUserHobby: option => $http.get("/v1/getUserHobby", option),
	hobbySave: option => $http.get("/v1/hobbySave", option),
	memberSave: option => $http.post("/v1/memberSave", option),
	mySave: option => $http.post("/v1/mySave", option),
}