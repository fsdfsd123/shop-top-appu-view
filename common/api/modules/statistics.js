import Vue from "vue";

const $http = Vue.prototype.$http
import $store from '@/config/store'

let startTime = 0;

function start() {
	console.log("start")
	startTime=Date.parse(new Date());
}

function end(url) {
	let time=Date.parse(new Date())-startTime;
	let userInfo=$store.state.userInfo;
	if(userInfo.id==null||userInfo.id==undefined||userInfo.id<0){
		return false;
	}else{
		/**
		 * mid
			int
			登录成功返回去的ID
			url
			string
			用户当前浏览地址
			time
		 */
		$http.get("/v1/statistics", {params:{mid:userInfo.id,url:url,time:time/1000}}).then(res => {
		
		})
	}
}

export default {
	start,
	end,
}
