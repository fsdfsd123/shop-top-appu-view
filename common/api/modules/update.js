import Vue from "vue";

const $http = Vue.prototype.$http

export default {
	checkVersion: options => $http.get('v1/getUpdate', options),
}
